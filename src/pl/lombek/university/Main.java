package pl.lombek.university;

import pl.lombek.university.menu.SystemMenuFactory;
import pl.lombek.university.menu.SystemMenuView;
import pl.lombek.university.security.domain.LoggedUser;
import pl.lombek.university.security.domain.LoginService;
import pl.lombek.university.security.domain.PasswordEncoder;
import pl.lombek.university.security.domain.PasswordEncoderImpl;
import pl.lombek.university.security.form.LoginForm;
import pl.lombek.university.subject.domain.FileSubjectRepository;
import pl.lombek.university.subject.domain.SubjectRepository;
import pl.lombek.university.user.domain.FileUserRepository;
import pl.lombek.university.user.domain.UserRepository;
import pl.lombek.university.user.domain.UserRole;

public class Main {

    public static void main(String[] args) {
        UserRepository userRepository = new FileUserRepository();
        PasswordEncoder passwordEncoder = new PasswordEncoderImpl();
        SubjectRepository subjectRepository = new FileSubjectRepository();

//        Optional<String> op3 = Optional.ofNullable("tekst");
//        op3 = Optional.empty();
//        op3.ifPresent(s -> System.out.println("wartosc = "+s));
//
//        String txt = op3.orElse("txt");
//        System.out.println(txt);

//        List<Subject> subjects = new ArrayList<>();
//        subjects.add(new Subject("przedmiot 1"));
//        subjects.add(new Subject("przedmiot 2"));
//        subjects.add(new Subject("przedmiot 3"));
//        subjects.add(new Subject("przedmiot 34"));
//        subjects.add(new Subject("przedmiot 4"));

//        List<Subject> collect = subjects.stream()
//            .filter(e -> e.getName().startsWith("przedmiot 3"))
//            .collect(Collectors.toList());

//        Consumer<String> consumer = s -> System.out.println(s + " fraza");
//        consumer.accept("189");

//        Function<String, Integer> c = s -> s.length();
//        System.out.println(c.apply("tekst"));
//        System.out.println(c.apply("Adam"));
//        System.out.println(c.apply("Kowal"));

//        Function<Integer, Integer> m = i -> i * i;
//        System.out.println(m.apply(10));
//        System.out.println(m.apply(5));

//        subjectRepository.insert(new Subject("Programowanie w języku Java"));
//        subjectRepository.insert(new Subject("Języki i automaty"));

//        Administrator administrator = new Administrator("Jan", "Kowalski",
//                "admin@kowalski.pl", "admin123");
//        userRepository.insert(administrator);
//        userRepository.insert(administrator);
//        userRepository.insert(administrator);
//        userRepository.insert(administrator);
//        userRepository.insert(administrator);

//        Teacher teacher = new Teacher("Witold",
//                "Nowak", "witold@nowak.com",
//                "witek", "mgr");
//        userRepository.insert(teacher);

//        Student student = new Student("Andrzej",
//                "Kowalski", "andrzej123@gmail.com",
//                "and3", 192052);
//        userRepository.insert(student);

        LoginService loginService = new LoginService(userRepository, passwordEncoder);
        LoginForm loginForm = new LoginForm(loginService);
        loginForm.initialize();

        LoggedUser loggedUser = loginService.getLoggedUser();
        System.out.println("Zalogowano się jako:");
        System.out.println(loggedUser.getDetails());

        UserRole userRole = loggedUser.getRole();
        SystemMenuFactory systemMenuFactory = new SystemMenuFactory();
        SystemMenuView systemMenuView = systemMenuFactory.getMenuView(
                userRole, userRepository, subjectRepository
        );
        systemMenuView.initialize();
    }

    /*
        userRepository.insert(teacher);
        userRepository.insert(teacher);

    List<User> users = userRepository.findAll();
        System.out.println("Lista użytkowników:");
        for (User user: users) {
        System.out.println(user);
    *
     */

}
