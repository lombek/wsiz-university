package pl.lombek.university.user.view;

import pl.lombek.university.console.ConsoleReader;
import pl.lombek.university.user.domain.Teacher;
import pl.lombek.university.user.domain.UserRepository;

public class TeacherDetailsView extends UserDetailsView<Teacher> {

    public TeacherDetailsView(UserRepository userRepository) {
        super(userRepository);
    }

    @Override
    protected String getTitle() {
        return "WSTAWIANIE NAUCZYCIELA";
    }

    @Override
    protected Teacher createUser() {
        System.out.println("Podaj imię:");
        String firstName = ConsoleReader.readString();

        System.out.println("Podaj nazwisko:");
        String lastName = ConsoleReader.readString();

        System.out.println("Podaj adres email:");
        String email = ConsoleReader.readString();

        System.out.println("Podaj hasło:");
        String password = ConsoleReader.readString();

        System.out.println("Podaj tytuł naukowy:");
        String academicDegree = ConsoleReader.readString();

        return new Teacher(
                firstName,
                lastName,
                email,
                password,
                academicDegree
        );
    }

}
