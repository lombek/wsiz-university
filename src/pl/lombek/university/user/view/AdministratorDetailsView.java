package pl.lombek.university.user.view;

import pl.lombek.university.console.ConsoleReader;
import pl.lombek.university.user.domain.Administrator;
import pl.lombek.university.user.domain.UserRepository;

public class AdministratorDetailsView extends UserDetailsView<Administrator> {

    public AdministratorDetailsView(UserRepository userRepository) {
        super(userRepository);
    }

    @Override
    protected String getTitle() {
        return "WSTAWIANIE ADMINISTRATORA";
    }

    @Override
    protected Administrator createUser() {
        System.out.println("Podaj imię:");
        String firstName = ConsoleReader.readString();

        System.out.println("Podaj nazwisko:");
        String lastName = ConsoleReader.readString();

        System.out.println("Podaj adres email:");
        String email = ConsoleReader.readString();

        System.out.println("Podaj hasło:");
        String password = ConsoleReader.readString();

        return new Administrator(
                firstName,
                lastName,
                email,
                password
        );
    }

}
