package pl.lombek.university.user.domain;

public class Student extends User {

    private long albumNumber;

    public Student(String firstName, String lastName, String email,
                   String password, long albumNumber) {
        super(firstName, lastName, email, password, UserRole.STUDENT);
        this.albumNumber = albumNumber;
    }

    @Override
    public Long getAlbumNumber() {
        return albumNumber;
    }

    @Override
    public String toString() {
        return super.toString()+" [nr albumu "+this.albumNumber+"]";
    }

}
