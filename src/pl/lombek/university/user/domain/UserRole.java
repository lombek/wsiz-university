package pl.lombek.university.user.domain;

public enum UserRole {

    STUDENT,
    TEACHER,
    ADMINISTRATOR;

    public String getTranslated() {
        String translated = null;
        switch (this) {
            case STUDENT:
                translated = "Student";
                break;
            case TEACHER:
                translated = "Nauczyciel";
                break;
            case ADMINISTRATOR:
                translated = "Administrator";
                break;
            default:
                throw new RuntimeException("Not supported role = "+this.name());
        }
        return translated;
    }

}
