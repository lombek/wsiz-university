package pl.lombek.university.user.domain;

import java.io.Serializable;

public abstract class User implements Serializable {

    protected String firstName;
    protected String lastName;
    protected String email;
    protected String password;
    private UserRole role;

    public User(String firstName, String lastName, String email, String password,
                UserRole role) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public UserRole getRole() {
        return role;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Long getAlbumNumber() {
        return null;
    }

    public String getAcademicDegree() {
        return null;
    }

}
