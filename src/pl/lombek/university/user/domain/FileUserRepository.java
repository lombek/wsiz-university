package pl.lombek.university.user.domain;

import pl.lombek.university.BaseFileRepository;

import java.util.ArrayList;
import java.util.List;

public class FileUserRepository extends BaseFileRepository<User> implements UserRepository {

    private static final String FILENAME = "users.bin";

    public FileUserRepository() {
        super(FILENAME);
    }

    @Override
    public void deleteByEmail(String email) {
        List<User> users = findAll();
        List<User> result = new ArrayList<>();
        for (User user: users) {
            if (!user.getEmail().equalsIgnoreCase(email)) {
                result.add(user);
            }
        }
        saveList(result);
    }

}
