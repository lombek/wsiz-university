package pl.lombek.university.console;

import java.util.Scanner;

public class ConsoleReader {

    public static String readString() {
        Scanner scanner = new Scanner(System.in);
        String result = scanner.next();
        return result;
    }

    public static long readLong() {
        Scanner scanner = new Scanner(System.in);
        long result = scanner.nextLong();
        return result;
    }

    public static void waitForEnter() {
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }

    public static int readInt() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

}
