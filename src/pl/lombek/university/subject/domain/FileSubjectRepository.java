package pl.lombek.university.subject.domain;

import pl.lombek.university.BaseFileRepository;

public class FileSubjectRepository extends BaseFileRepository<Subject>
    implements SubjectRepository {

  private static final String FILENAME = "subjects.bin";

  public FileSubjectRepository() {
    super(FILENAME);
  }

}
