package pl.lombek.university.subject.domain;

import java.util.List;

public interface SubjectRepository {

    void insert(Subject subject);

    List<Subject> findAll();

}
