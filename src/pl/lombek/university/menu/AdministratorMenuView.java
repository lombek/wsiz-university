package pl.lombek.university.menu;

import pl.lombek.university.console.ConsoleReader;
import pl.lombek.university.subject.domain.SubjectRepository;
import pl.lombek.university.subject.view.SubjectListView;
import pl.lombek.university.user.domain.UserRepository;
import pl.lombek.university.user.view.*;

public class AdministratorMenuView extends SystemMenuView {

    private final UserRepository userRepository;
    private final SubjectRepository subjectRepository;

    public AdministratorMenuView(
        UserRepository userRepository,
        SubjectRepository subjectRepository
    ) {
        this.userRepository = userRepository;
        this.subjectRepository = subjectRepository;
    }

    @Override
    public void initialize() {
        AdministratorMenuItem chosenItem;
        do {
            super.initialize();
            AdministratorMenuItem[] items = AdministratorMenuItem.values();
            for (AdministratorMenuItem item : items) {
                System.out.println(item.getTranslatedWithNumber());
            }
            System.out.print("Co chcesz zrobić? ");
            int number = ConsoleReader.readInt();
            chosenItem = AdministratorMenuItem.ofNumber(number);
            switch (chosenItem) {
                case USER_LIST:
                    UserListView userListView = new UserListView(userRepository);
                    userListView.initialize();
                    break;
                case ADMINISTRATOR_ADD:
                    AdministratorDetailsView administratorDetailsView = new AdministratorDetailsView(userRepository);
                    administratorDetailsView.initialize();
                    break;
                case TEACHER_ADD:
                    TeacherDetailsView teacherDetailsView = new TeacherDetailsView(userRepository);
                    teacherDetailsView.initialize();
                    break;
                case STUDENT_ADD:
                    StudentDetailsView studentDetailsView = new StudentDetailsView(userRepository);
                    studentDetailsView.initialize();
                    break;
                case USER_DELETE:
                    UserActionDeleteView userActionDeleteView = new UserActionDeleteView(userRepository);
                    userActionDeleteView.initialize();
                    break;
                case SUBJECT_LIST:
                    SubjectListView subjectListView = new SubjectListView(subjectRepository);
                    subjectListView.initialize();
                    break;
            }
        } while (chosenItem != AdministratorMenuItem.QUIT);
    }

    @Override
    protected String getTitle() {
        return "MENU ADMINISTRATORA";
    }


}
