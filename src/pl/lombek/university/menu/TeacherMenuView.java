package pl.lombek.university.menu;

public class TeacherMenuView extends SystemMenuView {

    @Override
    public void initialize() {
        super.initialize();
    }

    @Override
    protected String getTitle() {
        return "MENU NAUCZYCIELA";
    }

}
