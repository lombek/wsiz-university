package pl.lombek.university.menu;

public enum AdministratorMenuItem {

    USER_LIST(1),
    ADMINISTRATOR_ADD(2),
    TEACHER_ADD(3),
    STUDENT_ADD(4),
    USER_DELETE(5),
    SUBJECT_LIST(6),
    QUIT(7);

    private final int number;

    AdministratorMenuItem(int number) {
        this.number = number;
    }

    public String getTranslated() {
        switch (this) {
            case USER_LIST:
                return "lista użytkowników";
            case ADMINISTRATOR_ADD:
                return "dodaj administratora";
            case TEACHER_ADD:
                return "dodaj nauczyciela";
            case STUDENT_ADD:
                return "dodaj studenta";
            case USER_DELETE:
                return "usuń użytkownika";
            case SUBJECT_LIST:
                return "lista przedmiotów";
            case QUIT:
                return "wyjście z programu";
        }
        throw new RuntimeException("Unsupported position");
    }

    public String getTranslatedWithNumber() {
        return number + " - " + getTranslated();
    }

    public static AdministratorMenuItem ofNumber(int number) {
        for (AdministratorMenuItem item: values()) {
            if (item.number == number) {
                return item;
            }
        }
        return AdministratorMenuItem.QUIT;
    }

}
