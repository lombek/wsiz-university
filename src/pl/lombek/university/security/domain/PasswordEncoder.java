package pl.lombek.university.security.domain;

public interface PasswordEncoder {

    String encode(String password);

}
