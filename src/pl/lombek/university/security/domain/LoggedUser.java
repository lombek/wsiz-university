package pl.lombek.university.security.domain;

import pl.lombek.university.user.domain.UserRole;

public class LoggedUser {

    private String firstName;
    private String lastName;
    private String email;
    private UserRole role;

    public LoggedUser(String firstName, String lastName, String email, UserRole role) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.role = role;
    }

    public String getDetails() {
        return String.format("%s %s (%s)\nFunkcja: %s",
                firstName, lastName, email, role.getTranslated());
    }

    public UserRole getRole() {
        return role;
    }

}
