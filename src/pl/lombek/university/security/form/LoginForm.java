package pl.lombek.university.security.form;

import pl.lombek.university.BaseView;
import pl.lombek.university.console.ConsoleReader;
import pl.lombek.university.security.domain.BadCredentialsException;
import pl.lombek.university.security.domain.LoginService;
import pl.lombek.university.user.domain.User;

public class LoginForm extends BaseView {

    private final LoginService loginService;

    public LoginForm(LoginService loginService) {
        this.loginService = loginService;
    }

    public void initialize() {
        boolean batchEnabled = Boolean.parseBoolean(System.getProperty("university.login.enabled"));
        String email;
        String password;
        if (batchEnabled) {
            email = System.getProperty("university.login.email");
            password = System.getProperty("university.login.password");
        } else {
            super.initialize();

            System.out.println("Podaj adres email: ");
            email = ConsoleReader.readString();

            System.out.println("Podaj hasło: ");
            password = ConsoleReader.readString();
        }
        try {
            User user = loginService.checkCredentials(email, password);
            loginService.saveUser(user);
        } catch (BadCredentialsException ex) {
            System.out.println("Nieprawidłowe dane dostępowe");
            System.out.println("Naciśnij ENTER, aby kontynuować");
            ConsoleReader.waitForEnter();
            initialize();
        }
    }

    @Override
    protected String getTitle() {
        return "EKRAN LOGOWANIA";
    }

}
