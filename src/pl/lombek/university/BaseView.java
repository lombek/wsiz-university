package pl.lombek.university;

public abstract class BaseView {

    public void initialize() {
        System.out.println("===== "+getTitle()+" =====");
    }

    protected abstract String getTitle();

}
